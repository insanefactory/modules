from adafruit_hid.keyboard import Keyboard as AdaKeyboardHID

try: # TODO: Get this from resource
    import usb_hid
except:
    usb_hid = None


class ConsumerControl(int):
    pass

class KeyCode(int):
    pass


class KeyboardHID(AdaKeyboardHID):
    def led_on(self, led_code: int) -> bool:
        # Overwrite because Adafruid implementation is just stupid.
        led_status = getattr(self._keyboard_device, "get_last_received_report", None)
        if led_status == None:
            return None
        last_report = led_status()
        if (last_report is None):
            return None
        b = bytes(last_report)
        return bool(b[0] & led_code)


class KeyboardBase:

    def _keyboard_init_(self, resources={}, force_bt=False):
        self.bt = resources.get("Bluetooth")
        self.bluetooth = False
        self.keyboard = None
        if force_bt:
            self.start_bt()
            return
        try:
            self.keyboard = KeyboardHID(usb_hid.devices)
        except:
            self.start_bt()


    def start_bt(self):
        self.bt.request()
        self.bluetooth = True

    def get_keyboard(self):
        if self.keyboard:
            return self.keyboard
        if self.bluetooth:
            self.keyboard =  KeyboardHID(self.bt.get_hid().devices)
        try:
            self.keyboard = KeyboardHID(usb_hid.devices)
        except:
            self.keyboard = None # Return mock in future
        return self.keyboard