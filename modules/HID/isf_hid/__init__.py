from in_sane_factory.module import Module
from .keyboard import KeyboardInterface
from .keyboard_layer import KeyboardLayerInterface

class HIDModule(Module):
    ID = "isf_hid"
    INTERFACES = [KeyboardInterface, KeyboardLayerInterface]

HIDModule()