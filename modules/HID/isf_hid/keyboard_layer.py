from in_sane_factory.layer_interface import Layer
from in_sane_factory.action import Action
from adafruit_hid.keycode import Keycode
from adafruit_hid.consumer_control_code import ConsumerControlCode
from .helper import KeyCode, ConsumerControl, KeyboardBase


class KeyboardLayerInterface(Layer, KeyboardBase):

    def __init__(self, layouts):
        self.active_layer = 0
        self.layers = []
        self.layerPressAction = []
        self.layerReleaseAction = []
        self.create_layers(layouts)
        self._keyboard_init_()
    
    def check_layer(self, layer: int):
        return 0 <= layer < len(self.layers)

    def create_layers(self, layouts):
        depth = len(layouts)
        height = max(map(len, layouts))
        width = max(map(lambda layout: max(map(len, layout)), layouts))
        print(f"[D] Keyboard layout depth {depth}, height {height} and widht {width}")
        self.layers = [[[None for _ in range(width)] for _ in range(height)] for _ in range(depth)]
        for layout_index, layout_value in enumerate(layouts):
            for row_index, row_value in enumerate(layout_value):
                for key_index, key_value in enumerate(row_value):
                    if not key_value:
                        continue
                    value = getattr(Keycode, key_value, None)
                    if value:
                        self.layers[layout_index][row_index][key_index] = KeyCode(value)
                    value = getattr(ConsumerControlCode, key_value, None)
                    if value:
                        self.layers[layout_index][row_index][key_index] = ConsumerControl(value)
        self.layerPressAction = [None for _ in range(depth)]
        self.layerReleaseAction = [None for _ in range(depth)]

    def get_key_value(self, row, column):
        return self.layers[self.active_layer][row][column]

    def trigger(self, trigger: str):
        if trigger[0] == "p":
            self.press(*map(int, trigger[1:].split("x")))
        if trigger[0] == "r":
            self.release(*map(int, trigger[1:].split("x")))

    def check_trigger(self, trigger: str):
        if trigger[0] not in ["r", "p"]:
            return False
        row, column = map(int, trigger[1:].split("x"))
        return True

    def press(self, row, column):
        value = self.get_key_value(row, column)
        if isinstance(value, KeyCode):
            self.get_keyboard().press(int(value))
        if isinstance(value, tuple):
            if value[0]:
                value[0].run()
        if self.layerPressAction[self.active_layer]:
            self.layerPressAction[self.active_layer].run()

    def release(self, row, column):
        value = self.get_key_value(row, column)
        if isinstance(value, KeyCode):
            self.get_keyboard().release(int(value))
        if isinstance(value, tuple):
            if value[1]:
                value[1].run()
        if self.layerReleaseAction[self.active_layer]:
            self.layerReleaseAction[self.active_layer].run()

    def set_trigger(self, id, action: Action):
        parts = id.split("_")
        if parts[0] not in ["press", "release"]:
            return
        if parts[1] == "layer":
            layer = int(parts[2])
            if parts[0] == "press":
                self.layerPressAction[layer] = action
            else:
                self.layerReleaseAction[layer] = action
            return
        layer, row, column = map(int, parts[1:])
        original_value = self.layers[layer][row][column]
        index = int(parts[0] == "release")
        if isinstance(original_value, tuple):
            original_value = original_value[1 - index]
        else:
            original_value = None
        if index:
            self.layers[layer][row][column] = (original_value, action)
        else:
            self.layers[layer][row][column] = (action, original_value)

