from in_sane_factory.action import Action
from in_sane_factory.interface import Interface
try:
    import usb_hid
except:
    usb_hid = None
from adafruit_hid.keycode import Keycode
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from .helper import KeyboardHID


class Keypress(Action):
    def __init__(self, interface:KeyboardInterface, keycode):
        super().__init__(interface)
        self.code = keycode

    def run(self):
        self.interface.get_keyboard().press(self.code)


class Keyrelease(Keypress):
    def run(self):
        self.interface.get_keyboard().release(self.code)


class Keyclick(Keypress):
    def run(self):
        # self.interface.get_keyboard().send(self.code) # This is bad idea. `send` calls `release_all`
        self.interface.get_keyboard().press(self.code)
        self.interface.get_keyboard().release(self.code)


class Keyreleaseall(Action):
    def run(self):
        self.interface.get_keyboard().release_all()


class Keycombo(Action):
    def __init__(
        self, interface: KeyboardInterface, keys:list,
        press:bool=True, release:bool=True):
        super().__init__(interface)
        self.keys = keys
        self.press = press
        self.release = release

    def run(self):
        if self.press:
            for key in self.keys:
                self.interface.get_keyboard().press(key)
        if self.release:
            for key in reversed(self.keys):
                self.interface.get_keyboard().release(key)


class KeyType(Action):
    def __init__(self, interface: KeyboardInterface, text:str):
        super().__init__(interface)
        self.text = text

    def run(self):
        self.interface.get_layout().write(self.text)


class Lockset(Keypress):
    KEY_TO_LED = {
        Keycode.CAPS_LOCK: KeyboardHID.LED_CAPS_LOCK,
        Keycode.SCROLL_LOCK: KeyboardHID.LED_SCROLL_LOCK,
        Keycode.KEYPAD_NUMLOCK: KeyboardHID.LED_NUM_LOCK

    }
    def __init__(self, interface: KeyboardInterface, keycode, target_state : bool, clickOnNone:bool = False):
        super().__init__(interface, keycode)
        self.led_code = self.KEY_TO_LED.get(keycode)
        self.target = target_state
        self.clickOnNone = clickOnNone
        if not self.led_code:
            raise Exception(f"Unknown lock key {keycode}")
    def run(self):
        state = self.interface.get_keyboard().led_on(self.led_code)
        if state is None and self.clickOnNone:
            self.interface.get_keyboard().send(self.code)
        if state != self.target:
            self.interface.get_keyboard().send(self.code)


class KeyboardInterface(Interface):
    ID = "hid_keyboard"
    ACTIONS = {"press": Keypress, "release": Keyrelease, "click": Keyclick}
    REQUIRED_RESOURCES = ["Bluetooth"]

    def __init__(self, resources = {}, force_bt=False):
        self.bt = resources.get("Bluetooth")
        self.watchedLocks = {}
        self.bluetooth = False
        self.keyboard = None
        self.layout = None
        if force_bt:
            self.start_bt()
            return
        try:
            self.keyboard = KeyboardHID(usb_hid.devices)
        except:
            self.start_bt()

    def start_bt(self):
        self.bt.request()
        self.bluetooth = True

    def _num_locked(self):
        return self.get_keyboard().led_on(KeyboardHID.LED_NUM_LOCK)

    def get_layout(self):
        if self.layout:
            return self.layout
        self.layout = KeyboardLayoutUS( self.get_keyboard() )
        return self.layout

    def get_keyboard(self):
        if self.keyboard:
            return self.keyboard
        if self.bluetooth:
            self.keyboard =  KeyboardHID(self.bt.get_hid().devices)
        try:
            self.keyboard = KeyboardHID(usb_hid.devices)
        except:
            self.keyboard = None # Return mock in future
        return self.keyboard
    
    def create_action(self, type, params):
        if type == "combo":
            key_ids = params.get("keys")
            return Keycombo(
                self, [getattr(Keycode, code) for code in key_ids],
                press=params.get("press", True),
                release=params.get("release", True)
            )
        if type == "release_all":
            return Keyreleaseall(self)
        if type == "write":
            return KeyType(self, params.get("text", ""))
        key_id = params.get("key")
        key = getattr(Keycode, key_id)
        if type in self.ACTIONS.keys():
            return self.ACTIONS[type](self, keycode=key)
        elif type == "setlock":
            return Lockset(self, keycode=key, target_state=params.get("target_state", False))
    
    def update(self):
        for lock in self.watchedLocks:
            now_state = self.get_keyboard().led_on(lock)
            if now_state is None:
                continue
            if self.watchedLocks[lock][2] != now_state:
                self.watchedLocks[lock][2] = now_state
                if self.watchedLocks[lock][int(now_state)]:
                    self.watchedLocks[lock][int(now_state)].run()


    def set_trigger(self, id, action: Action):
        parts = id.split("_")
        lock = "_".join(parts[:2])
        if lock in [ "CAPS_LOCK", "SCROLL_LOCK", "KEYPAD_NUMLOCK" ]:
            if lock == "KEYPAD_NUMLOCK":
                led = KeyboardHID.LED_NUM_LOCK
            else:
                led = getattr(KeyboardHID, f"LED_{lock}")
            if led not in self.watchedLocks:
                self.watchedLocks[led] = [None, None, self.get_keyboard().led_on(led)]
            if parts[2] == "ON":
                self.watchedLocks[led][1] = action
            if parts[2] == "OFF":
                self.watchedLocks[led][0] = action
        return True