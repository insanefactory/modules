from adafruit_hid.keycode import Keycode
from adafruit_hid.consumer_control_code import ConsumerControlCode

class Helpers:
    @staticmethod
    def key_code(key_name):
        """
        return (consumer_code, key_code)
        """
        return getattr(Keycode, key_name)
