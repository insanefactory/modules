import busio
import board
from in_sane_factory.resource import ResourceBase


class I2C(ResourceBase):
    ID = "I2C"

    def __init__(self, sda, scl):
        scl_pin = getattr(board, scl)
        sda_pin = getattr(board, sda)
        self.instance = busio.I2C(scl=scl_pin, sda=sda_pin)
        if not self._start():
            raise Exception("Can't lock I2C.")
        super().__init__()

    def _start(self):
        return self.instance.try_lock()

    def scan(self):
        return self.instance.scan()

    def info(self):
        return {"devices": self.instance.scan()}

    def get_i2c(self):
        return self.instance