from in_sane_factory.module import Module
from .i2c import I2C

class I2cModule(Module):
    ID = "isf_i2c"
    RESOURCE = [I2C]

I2cModule()