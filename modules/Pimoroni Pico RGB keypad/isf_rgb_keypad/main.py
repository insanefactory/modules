from in_sane_factory.action import Action
from in_sane_factory.button import Button as ButtonBase
from in_sane_factory.interface import Interface
from pmk import PMK
from pmk.platform.rgbkeypadbase import RGBKeypadBase as Hardware

class PicoRGBKeypadInterface(Interface):
    ID = "rgp_keypad"

    def __init__(self):
        self.hardware = Hardware()
        self.pmk = PMK(self.hardware)
        super().__init__()
        self.buttons = [
            Button(index, self, None) for index in range(16)
        ]
        for key in self.pmk.keys:
            def p(key):
                # print("Key", key.number, "pressed")
                self.buttons[key.number].run_press()
            key.press_function = p
            def r(key):
                # print("Key", key.number, "released")
                self.buttons[key.number].run_release()
            key.release_function = r
        self.pmk.set_all(0, 0, 0)
    
    def update(self):
        self.pmk.update()
        return super().update()
    
    def update_color_map(self, map_pressed=None, map_idle=None):
        for button in self.buttons:
            pressed, idle = None, None
            if map_pressed and len(map_pressed) > button.number:
                pressed = map_pressed[button.number]
            if map_idle and len(map_idle) > button.number:
                idle = map_idle[button.number] 
            button.update_colors(idle=idle, pressed=pressed )

    def update_all_colors(self, idle=None, pressed=None):
        for button in self.buttons:
            button.update_colors(idle, pressed)

    def set_trigger(self, id, action: Action):
        trigger_type, index = id.split("_")
        index = int(index)
        if trigger_type == "bp":
            self.set_button_press_action(index, action)
            return True
        if trigger_type == "br":
            self.set_button_release_action(index, action)
            return True
        return False

    def set_button_press_action(self, button_number, action:Action):
        self.buttons[button_number].update_press_action(action)

    def set_button_release_action(self, button_number, action:Action):
        self.buttons[button_number].update_release_action(action)
    
    def create_action(self, type, params):
        if type=="set_color_map":
            return SetKeypadColors(self, idle=params.get("idle"), pressed=params.get("pressed"))


class Button(ButtonBase):
    def __init__(self, key_number, interface:PicoRGBKeypadInterface, press_action):
        self.number = key_number
        self.key = interface.pmk.keys[key_number]
        self.idle_color = (0, 0, 0)
        self.pressed_color = (40, 40, 40)
        super().__init__(interface, press_action)
    
    def update_colors(self, idle=None, pressed=None):
        if idle:
            self.idle_color = idle
        if pressed:
            self.pressed_color = pressed
        if self.key.pressed:
            self.key.set_led(*self.pressed_color)
        else:
            self.key.set_led(*self.idle_color)
        

    def run_press(self):
        # Led blink placeholder
        self.key.set_led(*self.pressed_color)
        return super().run_press()
    
    def run_release(self):
        self.key.set_led(*self.idle_color)
        return super().run_release()


class SetKeypadColors(Action):
    def __init__(self, interface: PicoRGBKeypadInterface, idle=None, pressed=None):
        super().__init__(interface)
        self.map_idle = idle
        self.map_pressed = pressed
    def run(self):
        self.interface.update_color_map(map_pressed=self.map_pressed, map_idle=self.map_idle)

class SetKeyColor(Action):
    pass

