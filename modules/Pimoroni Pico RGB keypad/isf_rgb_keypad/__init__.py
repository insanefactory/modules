from in_sane_factory.module import Module
from .main import PicoRGBKeypadInterface


class PimoroniKeypad(Module):
    ID = "isf_pimoroni_keypad"
    INTERFACES = [PicoRGBKeypadInterface]

PimoroniKeypad()