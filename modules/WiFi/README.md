# ISF WiFi module

## Resources example:
```json
"WIFI": {
    "authPairs": {
        "your_ssid": "your_password"
    }
}
```

## Config example
### Interface:
```json
"interfaces": {
        "request": {
            "module": "wifi",
            "interface": "http_requests"
        },
```
### Actions:
```json
"action": {
    "interface": "request", "type": "post", "args": {
        "url": "http://homeServer.local/api",
        "headers": { "content-type": "application/json" },
        "data": {"entity_id": "kitchenLight.on"}
    }
}
```