from in_sane_factory.module import Module
from .resource import WiFi as WiFiResource
from .requests import Requests

class WiFi(Module):
    ID = "isf_wifi"
    INTERFACES = [ Requests ]
    RESOURCE = [ WiFiResource ]

WiFi()