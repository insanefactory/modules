from in_sane_factory.interface import Interface
from in_sane_factory.action import Action
import adafruit_requests
import ssl
import json

class GetRequest(Action):
    def __init__(self, interface: Interface, url, headers):
        self.url = url
        self.headers = headers
        super().__init__(interface)

    def run(self):
        resp = self.interface.requests.get(self.url, headers=self.headers)
        if resp.status_code != 200:
            print("Requests status code:", resp.status_code)
            print("Requests text:", resp.text)
        resp.close()

class PostRequest(Action):
    def __init__(self, interface: Interface, url, headers={}, data={}):
        self.url = url
        self.headers = headers
        self.data = data
        super().__init__(interface)

    def run(self):
        resp = self.interface.requests.post(self.url, headers=self.headers, data=json.dumps(self.data))
        print("Requests status code:", resp.status_code)
        resp.close()


class Requests(Interface):
    ID = "http_requests"
    REQUIRED_RESOURCES = ["WIFI"]

    def __init__(self, resources={}, global_headers={}, global_url_base=""):
        self.wifi = resources.get("WIFI")
        self.requests = adafruit_requests.Session(self.wifi.getPool(), ssl.create_default_context()) 
        self.headers = global_headers
        self.url_base = global_url_base
        super().__init__()

    def create_action(self, type, params):
        url = self.url_base + params.get("url", "")
        headers = self.headers
        headers.update(params.get("headers")) 
        if type == "get":
            return GetRequest(self, url=url, headers=headers)
        if type == "post":
            return PostRequest(self, url=url, headers=headers, data=params.get("data", {}))
