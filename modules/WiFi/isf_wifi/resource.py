from in_sane_factory.resource import ResourceBase
import wifi
import socketpool

class WiFi(ResourceBase):
    ID = "WIFI"

    def __init__(self, authPairs):
        self.auth_pairs = authPairs
        self.pool = None
        self.connectWiFi()
        super().__init__()
    
    def connectWiFi(self):
        ssid = None
        for network in wifi.radio.start_scanning_networks():
            found_ssid = str(network.ssid, "utf-8")
            if found_ssid in self.auth_pairs.keys():
                ssid = found_ssid
                break
        wifi.radio.stop_scanning_networks()
        if not ssid:
            print("WiFi Resource: No know network find.")
            return
        wifi.radio.connect(ssid, self.auth_pairs[ssid])
        if wifi.radio.ipv4_gateway:
            print("WiFi", ssid, "connected.")
    
    def getPool(self):
        if wifi.radio.ipv4_gateway:
            return socketpool.SocketPool(wifi.radio)