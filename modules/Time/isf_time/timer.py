from in_sane_factory.interface import Interface
from in_sane_factory.action import Action
from time import monotonic_ns

class TimerInterface(Interface):
    ID = "timer"

    def __init__(self):
        super().__init__()
        self.timeouts = {}
        self.actions = {}
    
    def set_timeout(self, id, delay):
        if id not in self.actions.keys():
            raise Exception("Unknown action.")
        delay_ns = delay * 1000000 # ms to ns
        time_out = monotonic_ns() + delay_ns
        self.timeouts[id] = time_out
    
    def unset_timeout(self, id):
        if id in self.timeouts.keys():
            del(self.timeouts[id])
    
    def update(self):
        now = monotonic_ns()
        for id in self.timeouts.keys():
            if self.timeouts[id] < now:
                self.actions[id].run()
                del(self.timeouts[id])

    def set_trigger(self, id, action):
        self.actions[id] = action

    def create_action(self, type, params):
        action_id = params.get("id")
        if type == "set_timer":
            return SetTimedAction(self, action_id, params.get('timeout', 1))
        if type == "unset_timer":
            return UnsetTimedAction(self, action_id)

class SetTimedAction(Action):
    def __init__(self, interface: TimerInterface, action_id, timeout):
        self.id = action_id
        self.timeout = timeout
        super().__init__(interface)

    def run(self):
        self.interface.set_timeout(self.id, self.timeout)

class UnsetTimedAction(Action):
    def __init__(self, interface: TimerInterface, action_id):
        self.id = action_id
        super().__init__(interface)

    def run(self):
        self.interface.unset_timeout(self.id)