from in_sane_factory.module import Module
from .timer import TimerInterface
from .timed_macro import TimedMacroInterface

class TimeModule(Module):
    ID = "isf_time"
    INTERFACES=[TimerInterface, TimedMacroInterface]

TimeModule()