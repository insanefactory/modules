from in_sane_factory.interface import Interface
from in_sane_factory.action import Action
from time import monotonic_ns


class TimedMacroInterface(Interface):
    ID = "timed_macro"

    def __init__(self):
        self.start_time = 0
        self.last_run = 0
        self.actions = {}
        self.end_time = 0
        super().__init__()

    def set_trigger(self, id, action):
        target_time = 1000000 * id # ns => ms
        if target_time > self.end_time:
            self.end_time = target_time
        self.actions[target_time]=action

    def _stop(self):
        self.start_time = 0

    def _start(self):
        self.start_time = monotonic_ns()

    def update(self):
        now = monotonic_ns()
        relative_now = now - self.start_time
        relative_last_run = self.last_run - self.start_time
        if self.start_time:
            for action_time in self.actions.keys():
                if action_time > relative_last_run and action_time <= relative_now:
                    self.actions[action_time].run()
        self.last_run = now
        if self.end_time < relative_now:
            self.start_time = 0
    
    def create_action(self, type, params):
        if type == "start":
            return StartMacro(self)
        if type == "stop":
            return StopMacro(self)


class StartMacro(Action):
    def __init__(self, interface: TimedMacroInterface):
        super().__init__(interface)
    
    def run(self):
        self.interface._start()


class StopMacro(Action):
    def __init__(self, interface: TimedMacroInterface):
        super().__init__(interface)
    
    def run(self):
        self.interface._stop()