from in_sane_factory.module import Module
from .main import Neopixel

class Neopixels(Module):
    ID = "isf_neopixels"
    INTERFACES = [Neopixel]

Neopixels()