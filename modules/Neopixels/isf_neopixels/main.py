from in_sane_factory.interface import Interface
from in_sane_factory.action import Action
from time import monotonic_ns

import neopixel
import board


class Animation:
    def __init__(self, pixels_interface: Neopixel) -> None:
        self.interface = pixels_interface
    
    def isAlive(self):
        return False

    def update(self):
        pass


class Slide(Animation):
    def __init__(self, pixels_interface: Neopixel, new_color=[0,0,0], speed = 1) -> None:
        super().__init__(pixels_interface)
        self.start = monotonic_ns()
        self.new_color = new_color
        self.alive = True
        self.speed = speed
    
    def update(self):
        now = monotonic_ns()
        pixel_index = (self.speed*(now - self.start)) // (10**8)
        for pixel_id in range(self.interface.count):
            if pixel_id < pixel_index:
                self.interface.pixels[pixel_id] = self.new_color
                if pixel_id + 1 == self.interface.count:
                    self.alive = False
        self.interface.pixels.show()
    
    def isAlive(self):
        return self.alive


class Fade(Animation):
    def __init__(self, pixels_interface: Neopixel, new_color=[0,0,0], speed = 1) -> None:
        super().__init__(pixels_interface)
        self.new_color = new_color
        self.alive = True
        self.duration = 1 / speed
        self.end = (monotonic_ns() / (10**9)) + self.duration
    
    def update(self):
        now = monotonic_ns() / (10**9)
        fade_index = 1 - ((self.end - now) / self.duration)
        if fade_index < 0:
            fade_index = 0
        if fade_index > 1:
            fade_index = 1
        color = [int(x*fade_index) for x in self.new_color]
        self.interface.pixels.fill(color)
        if now > self.end:
            self.alive = False
        self.interface.pixels.show()
    
    def isAlive(self):
        return self.alive


class Fill(Action):
    def __init__(self, interface: Interface, color=[0,0,0]):
        super().__init__(interface)
        self.color = color

    def run(self):
        self.interface.pixels.fill(self.color)
        self.interface.changed = True

class SetEffect(Action):
    def __init__(self, interface: Interface, effect, effect_args) -> None:
        super().__init__(interface)
        self.effect = effect
        self.args = effect_args
    
    def run(self):
        self.interface.animation = self.effect(self.interface, **self.args)

class SetPixel(Action):
    def __init__(self, interface: Interface, pixel_id, color=[0,0,0]):
        super().__init__(interface)
        self.pixel_id = pixel_id
        self.color = color

    def run(self):
        self.interface.pixels[self.pixel_id] = self.color
        self.interface.changed = True

class SetColorMap(Action):
    def __init__(self, interface: Interface, color_map=[], pixel_offset=0):
        super().__init__(interface)
        for index, color in enumerate(color_map):
            if color is not None:
                color_map = color_map[index:]
                pixel_offset += index
                break
        self.map = color_map
        self.offset = pixel_offset

    def run(self):
        for index, color in enumerate(self.map):
            if color is not None:
                self.interface.pixels[index + self.offset] = color
        self.interface.changed = True


class Neopixel(Interface):
    ID = "neopixel"

    def __init__(self, pin, led_count: int, brightness:float = 0.2):
        self.pin = getattr(board, pin)
        self.count = led_count
        self.brightness = brightness
        self.pixels = neopixel.NeoPixel(self.pin, led_count, brightness=brightness, auto_write=False)
        self.pixels.fill((0, 0, 0))
        self.changed = True
        self.nextUpdate = 0
        self.animation = None
        super().__init__()
    
    def update(self):
        if self.animation and self.animation.isAlive():
            self.animation.update()
            return

        if self.changed or self.nextUpdate < monotonic_ns():
            self.pixels.show()
            self.changed = False
            self.nextUpdate = monotonic_ns() + 20*(10**9) # 20 sec
    
    def create_action(self, type, params):
        if type == "fill":
            return Fill(self, params.get("color", [0,0,0]))
        if type == "slide":
            return SetEffect(self, Slide, {
                "new_color": params.get("color", [0,0,0]),
                "speed": params.get("speed", 1)
            })
        if type == "fade":
            return SetEffect(self, Fade, {
                "new_color": params.get("color", [0,0,0]),
                "speed": params.get("speed", 1)
            })
        if type == "set":
            return SetPixel(self, params.get("pixel_id", 0), params.get("color", [0,0,0]))
        if type == "set_color_map":
            return SetColorMap(self, color_map=params.get("map", []), pixel_offset=params.get("pixel_offset", 0))