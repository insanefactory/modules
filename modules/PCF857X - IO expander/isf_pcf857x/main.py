from in_sane_factory.action import Action
from in_sane_factory.button import Button as ButtonBase
from in_sane_factory.interface import Interface
from pcf8575 import PCF8575

class SimplePCF8575(PCF8575):
    PIN_COUNT = 16

    def validate_pin(self, pin):
        return self._validate_pin(pin)

    def _validate_pin(self, pin):
        if not 0 <= pin < self.PIN_COUNT:
            raise ValueError('Invalid pin {}. Use 0-{}.'.format(pin, self.get_pin_count()-1))
        return pin

    def get_pin_count(self):
        return self.PIN_COUNT


class SimplePCF8574(SimplePCF8575):
    PIN_COUNT = 8


class PCF857XInterface(Interface):
    ID = "pcf857x"
    REQUIRED_RESOURCES = ["I2C"]

    def __init__(self, resources={}, adress=0x20, io_class=SimplePCF8575, negated=True):
        i2c = resources.get("I2C").get_i2c()
        self.io = io_class(i2c, adress)
        self.buttons = [Button(self, None, index, negated=negated) for index in range(self.io.get_pin_count())]
        super().__init__()
    
    def update(self):
        for button in self.buttons:
            button.update()
        return super().update()

    # CopyPaste from PicoRGBKeaypadInterface
    def set_button_press_action(self, button_number, action:Action):
        self.buttons[button_number].update_press_action(action)

    def set_button_release_action(self, button_number, action:Action):
        self.buttons[button_number].update_release_action(action)

    def set_trigger(self, id, action: Action):
        trigger_type, index = id.split("_")
        index = int(index)
        if trigger_type == "bp":
            self.set_button_press_action(index, action)
            return True
        if trigger_type == "br":
            self.set_button_release_action(index, action)
            return True
        return False

    def get_triggers(self):
        return [ ("ButtonPress", f"bp_{index}") for index in range(len(self.buttons)) ] + [ ("ButtonRelease", f"br_{index}") for index in range(len(self.buttons)) ] 

class PCF8575Interface(PCF857XInterface):
    ID = "pcf8575"
    def __init__(self, resources={}, adress=32):
        super().__init__(resources, adress, SimplePCF8575)


class PCF8574Interface(PCF857XInterface):
    ID = "pcf8574"
    def __init__(self, resources={}, adress=32):
        super().__init__(resources, adress, SimplePCF8574)

class Button(ButtonBase):
    def __init__(self, interface: PCF857XInterface, press_action: Action, button_id, negated=True):
        self.id = button_id
        self.state = None
        self.negated = negated
        super().__init__(interface, press_action)
    
    def getState(self):
        return self.negated != self.interface.io.pin(self.id)

    def update(self):
        if self.state is None:
            self.state = self.getState()
            return
        new_state = self.getState()
        if new_state != self.state:
            if new_state:
                self.run_press()
            else:
                self.run_release()
            self.state = new_state
