from in_sane_factory.module import Module
from .main import PCF8575Interface, PCF8574Interface

class PCF857XModule(Module):
    ID = "isf_pcf857x"
    INTERFACES = [PCF8575Interface, PCF8574Interface]

PCF857XModule()